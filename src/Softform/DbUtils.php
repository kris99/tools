<?php

namespace Softform;

/**
 * @author Krzysztof Czerwiński <kris@kris.biz.pl>
 * @copyright Softform
 */
class DbUtils
{
    static public $lastError;
    
    /**
     * Saves record.
     *
     * @param resource  $databaseHandle Database handle, created from pg_(p?)connect function.
     * @param string    $table          Table name.
     * @param array     $data           An array with data, keys are column names.
     * @param array     $uniqueFields   Unique fields (if found, updates row).
     * @param string    $idField        Id field name, default "id".
     * @param boolean   $update         If false, does not update.
     *
     * @return mixed False on error, $idField value otherwise.
     */
    static public function saveRecord($databaseHandle, $table, array $data, array $uniqueFields = array(), $idField = "id", $update = true)
    {
        self::$lastError = null;

        foreach ($data as $key => $value) {
            $bindData[$key] = self::postgresqlEscape($databaseHandle, $value);
        }

        foreach ($uniqueFields as $key) {
            $uniqueFieldsWhere[$key] = "$key = " . $bindData[$key];
        }

        $uniqueFieldsWhereString =
            !empty($uniqueFields)
                ? "WHERE " . implode(" AND ", $uniqueFieldsWhere)
                : ""
        ;

        $selectStatement = pg_query($databaseHandle, "SELECT $idField FROM $table $uniqueFieldsWhereString LIMIT 1");
        if (false === $selectStatement) {
            self::$lastError = pg_last_error($databaseHandle);
            $return          = false;
        } else {
            if (pg_num_rows($selectStatement) >= 1 && true === $update) {
                // Update.

                $results = pg_fetch_row($selectStatement);
                $id      = $results[0];
                $return  = $id;

                foreach ($bindData as $key => $value) {
                    $updateFieldsSet[$key] = "$key = $value";
                }
                $updateFieldsSetString = implode(", ", $updateFieldsSet);

                $updateStatement = pg_query($databaseHandle, "UPDATE $table SET $updateFieldsSetString WHERE $idField = $id");

                if (false === $updateStatement) {
                    self::$lastError = pg_last_error($databaseHandle);
                    $return          = false;
                }
            } else if (pg_num_rows($selectStatement) == 0) {
                // Insert.

                foreach ($bindData as $key => $value) {
                    $insertFieldsKeyList[$key] = $key;
                    $insertFieldsValues[$key]  = $value;
                }
                $insertFieldsKeyListString = implode(", ", $insertFieldsKeyList);
                $insertFieldsValuesString  = implode(", ", $insertFieldsValues);

                $insertStatement = pg_query($databaseHandle, "INSERT INTO $table ($insertFieldsKeyListString) VALUES ($insertFieldsValuesString) RETURNING $idField");

                if (false === $insertStatement) {
                    self::$lastError = pg_last_error($databaseHandle);
                    $return          = false;
                } else {
                    $results = pg_fetch_row($insertStatement);
                    $id      = $results[0];
                    $return  = $id;
                }
            } else {
                $results = pg_fetch_row($selectStatement);
                $id      = $results[0];
                $return  = $id;
            }
        }

        return $return;
    }
    
    static public function postgresqlEscape($databaseHandle, $data)
    {
        // TODO: If PHP >= 5.4.4, better to use $newValue = pg_escape_literal($value)
        if (is_bool($data)) {
            $newValue = $data ? 'true' : 'false';
        } else if (null === $data) {
            $newValue = 'null';
        } else {
            $newValue = "E'" . pg_escape_string($databaseHandle, (string) $data) . "'";
        }

        return $newValue;
    }
    
    
    static public function slug($name,$charset='UTF-8')
    {
        self::$lastError = null;
        
        if (function_exists('iconv')) {
            $out = @iconv($charset, 'ASCII//TRANSLIT', $name);
        }
        $out = preg_replace("/[^a-zA-Z0-9 -]/", "", $out);
        $out = strtolower($out);
        $out = str_replace(" ", "-", $out);
        for($i=1;$i<5;$i++)
        {
                $out = str_replace("--", "-", $out);            
        }
        return $out;
        
    }        
    
    static public function uniqueSlug($databaseHandle, $name, $table, $field, $charset='UTF-8')
    {
        self::$lastError = null;
        
        $out=self::slug($name);
        $query="SELECT ".pg_escape_string($field)." from ".pg_escape_string($table)." WHERE ".pg_escape_string($field)."='".pg_escape_string($out)."' ";
        $result = pg_query($databaseHandle, $query);        
        if(pg_num_rows($result) > 0 )
        {             
            for($i=2;$i<=100;$i++)
            {
                $out=self::slug($name).'-'.$i;
                $query="SELECT ".pg_escape_string($field)." from ".pg_escape_string($table)." WHERE ".pg_escape_string($field)."='".pg_escape_string($out)."' ";        
                $result = pg_query($databaseHandle, $query);
                if(pg_num_rows($result) == 0 )
                {
                        return($out);
                }
            }
        
        } else {
            return $out;
        }
        
        return false;
    }
    
    static public function safeSerialize($data)
    {
        return base64_encode(serialize($data));
    }   
    
    static public function safeUnserialize($data)
    {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data))
        {
            $data=base64_decode($data);
        }
        return unserialize($data);        
    }        
    
}

?>