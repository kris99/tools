<?php

namespace Softform;

/**
 * @author Krzysztof Czerwiński <kris@kris.biz.pl>
 * @copyright Softform
 */

class EasySpider {

    public static $interface = array();
  
    /**
     * standard setup
     * @var array
     */
    public static $settings=array(
        // Workaround: http://stackoverflow.com/questions/1486099/any-way-to-keep-curls-cookies-in-memory-and-not-on-disk
        'cookie'=>'/dev/null',							
        'useragent'=>array(
            'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:9.0) Gecko/20100101 Firefox/9.0',
            'Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20110814 Firefox/6.0',
            'Mozilla/5.0 (Windows NT 6.1; U; ru; rv:5.0.1.6) Gecko/20110501 Firefox/5.0.1 Firefox/5.0.1',
            'Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0 Firefox/5.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:2.0) Gecko/20110319 Firefox/4.0',
            'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.9) Gecko/20100915 Gentoo Firefox/3.6.9',
            'Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.4) Gecko/20100513 Firefox/3.6.4',
            'Mozilla/5.0 (X11; U; x86_64 Linux; en_GB, en_US; rv:1.9.2) Gecko/20100115 Firefox/3.6',
            'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)',
            'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; chromeframe/12.0.742.112)',
            'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0',
            'Mozilla/5.0 ( ; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; SLCC1; .NET CLR 1.1.4322)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; InfoPath.2)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; MS-RTC LM 8)',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 5.2)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; en-US)',
            'Mozilla/4.0 (compatible;MSIE 7.0;Windows NT 6.0)',
            'Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.1)',
            'Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00',
            'Opera/9.80 (Windows NT 5.1; U; en) Presto/2.9.168 Version/11.51',
            'Opera/9.80 (X11; Linux x86_64; U; bg) Presto/2.8.131 Version/11.10',
            'Mozilla/5.0 (Windows NT 6.1; U; nl; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 Opera 11.01',
            'Opera/9.80 (Windows NT 6.1 x64; U; en) Presto/2.7.62 Version/11.00',
            'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; de-at) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1',
            'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr-FR) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27',
            'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; zh-cn) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27',
            'Mozilla/5.0 (Windows; U; Windows NT 6.0; de-DE) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4',
            'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; ar) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4',
            'Mozilla/5.0 (Windows; U; Windows NT 6.0; ja-JP) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16',
            'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; zh-tw) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16',
            ),							
        'connectiontimeout'=>20,							
        'lowseedtimeout'=>60,							
        'timeout'=>90,							
        'followlocation'=>true,							
        'header'=>false,							
    );
  
    /**
     * curl handler
     * @var resource 
     */
    private static $ch;

    /**
     * selected ip
     */
    private static $ipSelected;
        
    public static $lastError;    
    
    /**
     * http get query
     * @param string $url - url
     * @param boolean $header - show http headers or not
     * @return string - query output 
     */
    function get($url,$header=false) {

        self::$settings['header']=$header;

        self::init();

        curl_setopt(self::$ch, CURLOPT_URL,$url);
        $data = curl_exec(self::$ch);

        return($data);	
    }  

    /**
     * http post query
     * @param string $url - url
     * @param array $post - array of post data
     * @param boolean $header - show http headers or not
     * @return string - query output 
     */
    function post($url,$post,$header=false) {

        self::$settings['header']=$header;

        self::init();

        foreach ($post as $key => $value) {
                $tmppost[] = $key.'='.urlencode($value);
        }
        $tmp_post = implode('&', $tmppost);

        curl_setopt(self::$ch, CURLOPT_POST, true);
        curl_setopt(self::$ch, CURLOPT_POSTFIELDS, $tmp_post);    

        curl_setopt(self::$ch, CURLOPT_URL,$url);
        $data = curl_exec(self::$ch);

        return($data);	
    }  
  
  
    /**
     * get http query info
     * @param string $url - url
     * @return array - array with curl_getinfo data
     */
    function getInfo($url) {

        self::$settings['header']=true;

        self::init();

        curl_setopt(self::$ch, CURLOPT_URL,$url);
        $tmp = curl_exec(self::$ch);
        $data = curl_getinfo(self::$ch);

        return($data);	
    }  
  
  
    /**
      * Inicjalizacja połączenia z curlem
      * Przy zdefiniowanych ip losujemy które ip wykorzystamy na dane połączenie
      */
  
    public function init()
    {
        self::$ch = curl_init();
        if(self::$settings['header']==true)
        {
           curl_setopt(self::$ch, CURLOPT_HEADER, 1);                 
        } else {   
           curl_setopt(self::$ch, CURLOPT_HEADER, 0);
        }
        curl_setopt(self::$ch, CURLOPT_RETURNTRANSFER, 1);	
        curl_setopt(self::$ch, CURLOPT_COOKIEJAR, self::$settings['cookie']);
        curl_setopt(self::$ch, CURLOPT_COOKIEFILE, self::$settings['cookie']);		
        curl_setopt(self::$ch, CURLOPT_CONNECTTIMEOUT, self::$settings['connectiontimeout']);			
        curl_setopt(self::$ch, CURLOPT_LOW_SPEED_TIME, self::$settings['lowseedtimeout']);		
        curl_setopt(self::$ch, CURLOPT_TIMEOUT, self::$settings['timeout']);	
        curl_setopt(self::$ch, CURLOPT_FOLLOWLOCATION, self::$settings['followlocation']);
        //losowanie ip
        if(count(self::$interface)>0)
        {
            self::$ipSelected=self::$interface[rand(0,count(self::$interface)-1)];
            curl_setopt(self::$ch, CURLOPT_INTERFACE, self::$ipSelected);
        }
        //losowanie useragenta
        self::$settings['useragent_selected']=self::$settings['useragent'][rand(0,count(self::$settings['useragent'])-1)];
        curl_setopt(self::$ch, CURLOPT_USERAGENT, self::$settings['useragent_selected']);
    }	
  
    public function parser($definition,$data)
    {

        $info=array();
        if(preg_match_all("/".$definition['main']['pattern']."/siD",$data,$m))
        {
            foreach($m[0] as $v)
            {
                $out=array();
                foreach($definition as $k1=>$v2)
                {
                    if($k1=='main') continue; //pomin blok z main
                    if(preg_match("/".$v2['pattern']."/siD",$v,$m1))
                    {
                        $item=trim($m1[1]);
                        $out[$k1]=$item;
                    } elseif($v2['required']===true) {
                        self::$lastError='we have a problem no '.$k1.' . Data not imported';

                        echo "---<pattern>---\n".$v2['pattern']."\n---<start>---\n".$v."\n---<end>---\n";

                        continue(2);
                    }
                }
                $info[]=$out;
            }
            return($info);
        } else {
            self::$lastError="we have a problem no records ";
            return false;
        }

    }    
  
}

?>